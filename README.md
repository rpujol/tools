# Tools

Ce projet contient l'ensemble des éléments extérieurs ( scripts Python et Bash ) utiles à l'installation de srsLTE, l'exécution des scénarios de mesures ou du traitement des fichiers de logs générés.

# chapitre5

Ce dossier contient l'ensemble des scripts bash et python de mise en pace des scénarios de mesures utilisés pour la production du chapitre 5 de la thèse.

# traitement

Ce dossier contient l'ensemble des scripts bash et python utilisés pour le traitement des données.
