#/bin/bash

echo "Put your inria gitlab password as argument #1"
mv srsuescript.sh ..
add-apt-repository ppa:ettusresearch/uhd
apt-get update
apt-get install cmake libfftw3-dev libmbedtls-dev libboost-program-options-dev libconfig++-dev
apt-get install libsctp-dev libuhd-dev libuhd003 uhd-host git iperf cpufrequtils
echo "GOVERNOR=\"performance\"" > /etc/default/cpufrequtils
sudo systemctl restart cpufrequtils
uhd_images_downloader
cd /home/ubuntu
chmod 777 srsuescript.sh
git clone https://rpujol:$1@gitlab.inria.fr/rpujol/srsLTE
cd srsLTE
mkdir build
cd build
cmake ../
make
make install
srslte_install_configs.sh user
ldconfig
nmcli networking off
echo done



