#!/usr/bin/env python3

import multiprocessing
import subprocess
import signal
import socket
import time
import sys
import os

#IMPORTANT GLOBAL VARIABLE / CONFIG PANEL
SERVER_ADDRESS = '192.168.0.21'
UE_ADDRESS = '192.168.0.24'
EPC_ADDRESS = '192.168.0.21'
EXPERIMENTATION_PATH = "Experimentations"
CURRENT_EXPERIMENTATION = "5UE1ENBDownlink10s"
PRB_SET = [15, 25, 50, 100]
GAIN_SET = [60, 65, 70, 75, 80, 85, 90]
MAGIC_BOUCLE_NUMBER = 10
IPERF_TIME = 10

#MAIN
if __name__ == "__main__":

    #INIT NETWORK
    os.system( "ifconfig eth2 down" )
    os.system( "ifconfig eth1 down" )
    os.system( "ifconfig eth0 up" )
    os.system( "ifconfig eth0 " + UE_ADDRESS + "/24" )
    os.system( "route add -net 192.168.0.0/24 gw " + UE_ADDRESS )
    
    #Other INIT
    data = b'NULL'

    for prb in PRB_SET:

        for gain in GAIN_SET:

            for current_cycle in range(MAGIC_BOUCLE_NUMBER):

                # Create a TCP/IP socket to connect to AgoraTelecomServer.py
                sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

                # Connect the socket to the port where the server is listening
                server_address = (SERVER_ADDRESS, 9303)
                print('connecting to {} port {}'.format(*server_address))
                sock.connect(server_address)

                try:

                    # Send data
                    message = b'MeUE'
                    sock.sendall(message)

                    # Receive data, must be length < 16
                    data = sock.recv(16)

                finally:
                    sock.close()

                if data == b'MeUE':

                    #Going Experimention Directory
                    os.chdir(os.getenv("HOME"))      
                    try:
                        os.mkdir(EXPERIMENTATION_PATH)
                    except:
                        print(EXPERIMENTATION_PATH + " existe déjà")

                    os.chdir(EXPERIMENTATION_PATH)
                    #Going Experimention Directory/Second                    
                    try:
                        os.mkdir(CURRENT_EXPERIMENTATION)
                    except:
                        print(CURRENT_EXPERIMENTATION + " existe déjà")

                    os.chdir(CURRENT_EXPERIMENTATION)
                    #Going Experimention Directory/Second/Third
                    try:
                        os.mkdir(str(prb) + "prb" + str(gain) + "gain")
                    except:
                        print(str(prb) + "prb" + str(gain) + "gain" + " existe déjà")

                    os.chdir(str(prb) + "prb" + str(gain) + "gain")
                    #Going Experimention Directory/Second/Third/Fourth
                    try:
                        os.mkdir("run"+ str(current_cycle))
                    except:
                        print("run"+ str(current_cycle) + " existe déjà")

                    os.chdir("run"+ str(current_cycle))

                    #Subprocess iperfserver d'écoute de client
                    fiperf = open('./iperfserver.txt', 'w')
                    args = [ "iperf", "-s", "-p", "5002" ]
                    iperfSERVER = subprocess.Popen(args, stdout = fiperf)

                    #Subprocess srsUE via srsUEscript.sh
                    os.system("/home/ubuntu/srsuescript.sh " + UE_ADDRESS)
                    fue = open('./ue.txt', 'w')
                    args = [ "srsue",
                        # "--rat.eutra.dl_earfcn",
                        # "2800,2900,3000,3100,3200,3300,3400",
                        "--usim.imsi",
                        "001010123456792",
                        "--usim.imei",
                        "353490069873320",
                        "--usim.k",
                        "00112233445566778899aabbccddeeee",
                        "--log.all_level",
                        "none",
                        "--log.rrc_level",
                        "info",
                        "--log.phy_level",
                        "info",
                        "--log.filename",
                        "./ue.log",
                        "--general.metrics_csv_enable",
                        "1",
                        "--general.metrics_csv_filename",
                        "./metrics.csv"]
                    print(args)
                    time.sleep(12)
                    srsUE = subprocess.Popen(args, stdout = fue)

                    # Create a TCP/IP socket server to receive from AgoraTeleocmEPC
                    sockreply = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                    sockreply.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

                    # Bind the socket to the port on this UE machine
                    server_address = (UE_ADDRESS, 9306)
                    print('starting up on {} port {}'.format(*server_address))
                    sockreply.bind(server_address)

                    # Listen for incoming connections
                    sockreply.listen(1)

                    connection, client_address = sockreply.accept()
                    try:
                        # Receive the data in small chunks and retransmit it
                        data = connection.recv(16)
                        if data:
                            if data == b'MeDoneEPC':
                                print("EPC and iperfClient are done")
                    finally:
                        # Clean up the connection
                        connection.close()

                    #Join
                    iperfSERVER.kill()
                    srsUE.kill()
                    fue.close()
                    fiperf.close()

                    #Le temps que tout se ferme correctement
                    time.sleep(3)
