#!/usr/bin/env python3

import multiprocessing
import subprocess
import signal
import socket
import time
import sys
import os

#IMPORTANT GLOBAL VARIABLE / CONFIG PANEL
SERVER_ADDRESS = '192.168.0.21'
ENB_ADRESS = '192.168.0.22'
UE_ADDRESS = '192.168.0.23'
EPC_ADDRESS = '192.168.0.21'
EXPERIMENTATION_PATH = "Experimentations"
CURRENT_EXPERIMENTATION = "TestIso"
EARFCN = "3000"        # --rf.dl_earfcn                        Downlink EARFCN
ENB_ID = "0x2"         # --enb.enb_id arg (=0x0)               eNodeB ID
ENB_NAME = "srsenb02"  # --enb.name arg (=srsenb01)            eNodeB Name
PRB_SET = [15, 25, 50, 100]
GAIN_SET = [60, 65, 70, 75, 80, 85, 90]
MAGIC_BOUCLE_NUMBER = 20
IPERF_TIME = 10

#MAIN
if __name__ == "__main__":

    #INIT NETWORK
    os.system( "ifconfig eth2 down" )
    os.system( "ifconfig eth1 down" )
    os.system( "ifconfig eth0 up" )
    os.system( "ifconfig eth0 " + ENB_ADRESS + "/24" )
    os.system( "route add -net 192.168.0.0/24 gw " + ENB_ADRESS )

    #Other INIT
    data = b'NULL'

    for prb in PRB_SET:

        for gain in GAIN_SET:

            for current_cycle in range(MAGIC_BOUCLE_NUMBER):

                # Create a TCP/IP socket to connect to AgoraTelecomServer.py
                sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

                # Connect the socket to the port where the server is listening
                server_address = (SERVER_ADDRESS, 9303)
                print('connecting to {} port {}'.format(*server_address))
                sock.connect(server_address)

                try:

                    # Send data
                    message = b'MeENB'
                    sock.sendall(message)

                    # Receive data, must be length < 16
                    data = sock.recv(16)

                finally:
                    sock.close()

                if data == b'MeENB':

                    #Going Experimention Directory
                    os.chdir(os.getenv("HOME"))    
                    try:
                        os.mkdir(EXPERIMENTATION_PATH)
                    except:
                        print(EXPERIMENTATION_PATH + " existe déjà")

                    os.chdir(EXPERIMENTATION_PATH)
                    #Going Experimention Directory/Second                    
                    try:
                        os.mkdir(CURRENT_EXPERIMENTATION)
                    except:
                        print(CURRENT_EXPERIMENTATION + " existe déjà")

                    os.chdir(CURRENT_EXPERIMENTATION)
                    #Going Experimention Directory/Second/Third
                    try:
                        os.mkdir(str(prb) + "prb" + str(gain) + "gain")
                    except:
                        print(str(prb) + "prb" + str(gain) + "gain" + " existe déjà")

                    os.chdir(str(prb) + "prb" + str(gain) + "gain")
                    #Going Experimention Directory/Second/Third/Fourth
                    try:
                        os.mkdir("run"+ str(current_cycle))
                    except:
                        print("run"+ str(current_cycle) + " existe déjà")

                    os.chdir("run"+ str(current_cycle))

                    #Subprocess srsENB
                    fenb = open('./enb.txt', 'w')
                    args = [ "srsenb",
                    	"--enb.enb_id",
                    	ENB_ID,
                    	"--enb.name",
                    	ENB_NAME,
                        "--enb.mme_addr",
                        EPC_ADDRESS,
                        "--enb.gtp_bind_addr",
                        ENB_ADRESS,
                        "--enb.s1c_bind_addr",
                        ENB_ADRESS,
                        "--enb_files.sib_conf",
                        "/root/.config/srslte/sib.conf",
                        "--enb_files.rr_conf",
                        "/root/.config/srslte/rr.conf",
                        "--enb_files.drb_conf",
                        "/root/.config/srslte/drb.conf",
                        "--rf.dl_earfcn",
                        EARFCN,
                        "--enb.n_prb",
                        str(prb),
                        "--rf.tx_gain",
                        str(gain),
                        "--log.all_level",
                        "none",
                        "--log.rrc_level",
                        "info",
                        "--log.phy_level",
                        "info",
                        "--log.filename",
                        "./enb.log",
                        "--expert.metrics_csv_enable",
                        "1",
                        "--expert.metrics_csv_filename",
                        "./metrics.csv"]
                    print(args)
                    time.sleep (10)
                    srsENB = subprocess.Popen(args, stdout = fenb)

                    # Create a TCP/IP socket server to receive from AgoraTelecomEPC
                    sockreply = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                    sockreply.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

                    # Bind the socket to the port on this UE machine
                    server_address = (ENB_ADRESS, 9306)
                    print('starting up on {} port {}'.format(*server_address))
                    sockreply.bind(server_address)

                    # Listen for incoming connections
                    sockreply.listen(1)

                    connection, client_address = sockreply.accept()
                    try:
                        # Receive the data in small chunks and retransmit it
                        data = connection.recv(16)
                        if data:
                            if data == b'MeDoneEPC':
                                print("EPC and iperfClient are done")
                    finally:
                        # Clean up the connection
                        connection.close()

                    #Join
                    srsENB.kill()
                    fenb.close()

                    #Le temps que tout se ferme correctement
                    time.sleep(10)