#/bin/bash

#Check for sudo rights
sudo -v || exit

#Shutdown interfaces
ifconfig eth0 $1/24
ifconfig eth1 down
ifconfig eth2 down
sudo echo "\
auto lo
iface lo inet loopback
iface tun_srsue inet dhcp

#interface srsue avec ue.conf
allow-hotplug tun_srsue
iface tun_srsue inet manual
gateway 172.16.0.1 
up route add -net 172.16.0.0 netmask 255.255.255.0 gw 172.16.0.1 tun_srsue" > /etc/network/interfaces

sudo /etc/init.d/network-manager restart
sleep 1

sudo echo "nameserver 8.8.8.8" > lol
sudo mv lol /etc/resolv.conf
ifconfig eth1 down
ifconfig eth2 down

#Lunch srsLTE
#sudo srsue ~/Experimentations/confsrs/ue1.conf

