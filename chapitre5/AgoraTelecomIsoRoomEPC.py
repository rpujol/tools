#!/usr/bin/env python3

import multiprocessing
import subprocess
import signal
import socket
import time
import sys
import os

#IMPORTANT GLOBAL VARIABLE / CONFIG PANEL
SERVER_ADDRESS = '192.168.0.21'
EPC_ADDRESS = '192.168.0.21'
EXPERIMENTATION_PATH = "Experimentations"
CURRENT_EXPERIMENTATION = "5UE1ENBDownlink10s"
EARFCN = "2800"        # --rf.dl_earfcn                        Downlink EARFCN
ENB_ID = "0x1"         # --enb.enb_id arg (=0x0)               eNodeB ID
ENB_NAME = "srsenb01"  # --enb.name arg (=srsenb01)            eNodeB Name
PRB_SET = [15, 25, 50, 100]
GAIN_SET = [60, 65, 70, 75, 80, 85, 90]
MAGIC_BOUCLE_NUMBER = 10
IPERF_TIME = 10



def iperf_client(ip, port, delay):
    
    #set delay to let UEs build and delay iperf exec
    time.sleep(25 + delay)
    print(os.getcwd())
    #Launch iperf client side : iperf -c 172.16.0.2 -d -t 20 > iperf0.txt
    fperf = open("./iperfclient"+ip+".txt", 'w')
    args = ["iperf", "-c", ip, "-t", str(IPERF_TIME), "-p", str(port)]
    try:
        perf = subprocess.Popen(args, stdout = fperf)
        perf.wait(timeout = (IPERF_TIME + 10))
    except:
        perf.kill()
        print("probleme iperf timeout")
    finally:
        fperf.close()
    time.sleep(5)
    print("iperf done")

#MAIN
if __name__ == "__main__":

    #INIT NETWORK
    os.system( "ifconfig eth2 down" )
    os.system( "ifconfig eth1 down" )
    os.system( "ifconfig eth0 up" )
    os.system( "ifconfig eth0 " + EPC_ADDRESS + "/24" )
    os.system( "route add -net 192.168.0.0/24 gw " + EPC_ADDRESS )
    os.system( "/home/ubuntu/srsRAN-release_19_12/srsepc/srsepc_if_masq.sh eth1" )


    #Other INIT
    data = b'NULL'
    iperf_pid = []

    #Fonction à appeler lorsque le signal SIGINT est reçu, kill les processus fils
    def handler(sig, frame):
        if sig == signal.SIGINT:
            for iperf in iperf_pid :
                try :
                    os.kill(iperf, signal.SIGKILL)
                except :
                    print(str(iperf) + " already killed")
            os.kill(os.getpid(), signal.SIGKILL)

    #Détournement du signal SIGINT
    signal.signal(signal.SIGINT, handler)

    for prb in PRB_SET:

        for gain in GAIN_SET:

            for current_cycle in range(MAGIC_BOUCLE_NUMBER):

                # Create a TCP/IP socket
                sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

                # Connect the socket to the port where the server is listening
                server_address = (SERVER_ADDRESS, 9303)
                print('connecting to {} port {}'.format(*server_address))
                sock.connect(server_address)

                try:
                    # Send data
                    message = b'MeEPC'
                    sock.sendall(message)

                    # Receive data, must be length < 16
                    data = sock.recv(16)

                finally:
                    sock.close()

                if data == b'MeEPC':
                       
                    #Going Experimention Directory
                    os.chdir(os.getenv("HOME"))    
                    try:
                        os.mkdir(EXPERIMENTATION_PATH)
                    except:
                        print(EXPERIMENTATION_PATH + " existe déjà")

                    os.chdir(EXPERIMENTATION_PATH)
                    #Going Experimention Directory/Second                    
                    try:
                        os.mkdir(CURRENT_EXPERIMENTATION)
                    except:
                        print(CURRENT_EXPERIMENTATION + " existe déjà")

                    os.chdir(CURRENT_EXPERIMENTATION)
                    #Going Experimention Directory/Second/Third
                    try:
                        os.mkdir(str(prb) + "prb" + str(gain) + "gain")
                    except:
                        print(str(prb) + "prb" + str(gain) + "gain" + " existe déjà")

                    os.chdir(str(prb) + "prb" + str(gain) + "gain")
                    #Going Experimention Directory/Second/Third/Fourth
                    try:
                        os.mkdir("run"+ str(current_cycle))
                    except:
                        print("run"+ str(current_cycle) + " existe déjà")

                    os.chdir("run"+ str(current_cycle))


                    ############################################################################################
                    ############################################################################################
                    ############################################################################################
                    ############################################################################################
                    #######################################      CORE      #####################################
                    ############################################################################################
                    ############################################################################################
                    ############################################################################################
                    ############################################################################################
                    #Création de processus fils d'écoute de client
                    iperfCLIENT = multiprocessing.Process(target=iperf_client, args=("172.16.0.2", 5002, 0))
                    iperfCLIENT.start()
                    iperfCLIENT2 = multiprocessing.Process(target=iperf_client, args=("172.16.0.3", 5002, 0))
                    iperfCLIENT2.start()
                    iperfCLIENT3 = multiprocessing.Process(target=iperf_client, args=("172.16.0.4", 5002, 0))
                    iperfCLIENT3.start()
                    iperfCLIENT4 = multiprocessing.Process(target=iperf_client, args=("172.16.0.5", 5002, 0))
                    iperfCLIENT4.start()
                    iperfCLIENT5 = multiprocessing.Process(target=iperf_client, args=("172.16.0.6", 5002, 0))
                    iperfCLIENT5.start()

                    #Récupération du pid du fils créé
                    iperf_pid.append(iperfCLIENT.pid)
                    iperf_pid.append(iperfCLIENT2.pid)
                    iperf_pid.append(iperfCLIENT3.pid)
                    iperf_pid.append(iperfCLIENT4.pid)
                    iperf_pid.append(iperfCLIENT5.pid)

                    #Subprocess srsEPC
                    fepc = open('./epc.txt', 'w')
                    os.system( "/home/ubuntu/srsRAN-release_19_12/srsepc/srsepc_if_masq.sh eth1" )
                    args = [ "srsepc",
                        "--mme.mme_bind_addr",
                        EPC_ADDRESS,
                        "--spgw.gtpu_bind_addr",
                        EPC_ADDRESS,
                        "--hss.db_file",
                        "/root/.config/srslte/user_db.csv"]
                    srsEPC = subprocess.Popen(args, stdout = fepc )

                    #time.sleep(1)

                    # #Launch srsENB
                    # fenb = open('./enb.txt', 'w')
                    # args = [ "srsenb",
                    #     "--enb.enb_id",
                    #     ENB_ID,
                    #     "--enb.name",
                    #     ENB_CELL_ID,
                    #     "--enb.mme_addr",
                    #     EPC_ADDRESS,
                    #     "--enb.gtp_bind_addr",
                    #     EPC_ADDRESS,
                    #     "--enb.s1c_bind_addr",
                    #     EPC_ADDRESS,
                    #     "--enb_files.sib_conf",
                    #     "/root/.config/srslte/sib.conf",
                    #     "--enb_files.rr_conf",
                    #     "/root/.config/srslte/rr.conf",
                    #     "--enb_files.drb_conf",
                    #     "/root/.config/srslte/drb.conf",
                    #     "--rf.dl_earfcn",
                    #     EARFCN,
                    #     "--enb.n_prb",
                    #     str(prb),
                    #     "--rf.tx_gain",
                    #     str(gain),
                    #     "--log.all_level",
                    #     "none",
                    #     "--log.rrc_level",
                    #     "info",
                    #     "--log.phy_level",
                    #     "info",
                    #     "--log.filename",
                    #     "./enb.log",
                    #     "--expert.metrics_csv_enable",
                    #     "1",
                    #     "--expert.metrics_csv_filename",
                    #     "./metrics.csv"]
                    # print(args)
                    # srsENB = subprocess.Popen(args, stdout = fenb )

                    #Join
                    iperfCLIENT.join()
                    iperfCLIENT2.join()
                    iperfCLIENT3.join()
                    iperfCLIENT4.join()
                    iperfCLIENT5.join()
                    iperf_pid.pop(0)
                    iperf_pid.pop(0)
                    iperf_pid.pop(0)
                    iperf_pid.pop(0)
                    iperf_pid.pop(0)
                    srsEPC.kill()
                    # srsENB.kill()
                    fepc.close()
                    # fenb.close()
                    ############################################################################################
                    ############################################################################################
                    ############################################################################################
                    ############################################################################################
                    ############################################################################################
                    ############################################################################################
                    ############################################################################################
                    ############################################################################################
                    ############################################################################################

                    # Create a TCP/IP socket
                    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

                    # Connect the socket to the port where the server is listening
                    server_address = (SERVER_ADDRESS, 9303)
                    print('connecting to {} port {}'.format(*server_address))
                    sock.connect(server_address)

                    try:

                        # Send data
                        message = b'MeDoneEPC'
                        sock.sendall(message)

                        # Receive data, must be length < 16
                        data = sock.recv(16)

                    finally:
                        sock.close()

                # srsEPC.join()
                # srsENB.join()