#!/usr/bin/env python3

import multiprocessing
import signal
import socket
import sys
import os

#IMPORTANT GLOBAL VARIABLE / CONFIG PANEL
SERVER_ADDRESS = '192.168.0.21'
CLIENT_ADDRESSES = ['192.168.0.22','192.168.0.23','192.168.0.24','192.168.0.25','192.168.0.26', '192.168.0.27'] #Do not add EPC ADDRESS here, done automatically
EPC_ADDRESS = '192.168.0.21'
PORT_INIT = 9306
EXPERIMENTATION_PATH = "Experimentations"
CURRENT_EXPERIMENTATION = "5UE1ENBDownlink10s"
PRB_SET = [15, 25, 50, 100]
GAIN_SET = [60, 65, 70, 75, 80, 85, 90]
MAGIC_BOUCLE_NUMBER = 10

def listeningProcess(sock, bar):

    # Listen for incoming connections
    sock.listen(1)

    check = True

    while check:
        # Wait for a connection
        connection, client_address = sock.accept()
        try:
            print(multiprocessing.current_process())
            print('connection from', client_address)

            # Receive the data in small chunks and retransmit it
            data = connection.recv(16)
            if data:
                if data == b'MeEPC':
                    print("EPC arrived")
                    check = False
                    bar.wait()
                    connection.sendall(data)
                elif data == b'MeENB':
                    print("ENB arrived")
                    check = False
                    bar.wait()
                    connection.sendall(data)
                elif data == b'MeUE':
                    print("UE arrived")
                    check = False
                    bar.wait()
                    connection.sendall(data)
                elif data == b'MeDoneEPC':

                    check = False
                    for client in CLIENT_ADDRESSES :
                        # Create a TCP/IP socket to relay message to all the clients
                        sockreply = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                        # Connect the socket to the port where the clients are listening
                        server_address = (client, PORT_INIT)
                        print('connecting to {} port {}'.format(*server_address))
                        sockreply.connect(server_address)

                        try:
                            # Send data
                            message = b'MeDoneEPC'
                            sockreply.sendall(message)

                        finally:
                            sockreply.close()

                    connection.sendall(message)
            else:
                print('no data from', client_address)

        finally:
            # Clean up the connection
            connection.close()

#MAIN
if __name__ == "__main__":

    #INIT NETWORK
    os.system( "ifconfig eth2 down" )
    os.system( "ifconfig eth1 down" )
    os.system( "ifconfig eth0 up" )
    os.system( "ifconfig eth0 " + SERVER_ADDRESS + "/24" )
    os.system( "route add -net 192.168.0.0 netmask 255.255.255.0 gw " + SERVER_ADDRESS )

    #Create Barrier object
    bar = multiprocessing.Barrier(len(CLIENT_ADDRESSES) + 1)

    #Global
    listening_pid = []

    #Fonction à appeler lorsque le signal SIGINT est reçu, kill les processus fils
    def handler(sig, frame):
        if sig == signal.SIGINT:
            for listening in listening_pid :
                try :
                    os.kill(listening, signal.SIGKILL)
                except :
                    print(str(listening) + " already killed")
            os.kill(os.getpid(), signal.SIGKILL)

    #Détournement du signal SIGINT
    signal.signal(signal.SIGINT, handler)

    # Create a TCP/IP socket
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

    # Bind the socket to the port
    server_address = (SERVER_ADDRESS, 9303)
    print('starting up on {} port {}'.format(*server_address))
    sock.bind(server_address)

    for prb in PRB_SET:

        for gain in GAIN_SET:

            for current_cycle in range(MAGIC_BOUCLE_NUMBER):

                #Création de processus fils d'écoute de client
                procs = [multiprocessing.Process(target=listeningProcess, args=(sock, bar)) for _ in range(len(CLIENT_ADDRESSES)+1)]
                for proc in procs :
                    proc.start()
                    #Récupération du pid du fils créé
                    listening_pid.append(proc.pid)

                #Waiting for both clients to connect
                for proc in procs :
                    proc.join()
                    listening_pid.pop(0)
                print("UEs, ENBs and EPC en cours d'exec.")

                #Création de processus fils d'écoute de EPC après la bataille
                p=multiprocessing.Process(target=listeningProcess, args=(sock, bar))
                p.start()

                #Récupération du pid du fils créé après la bataille
                listening_pid.append(p.pid)
                listening_pid.pop(0)

                #Waiting for result of la bataille
                p.join()
                print("END of cycle: " + str(current_cycle) + " of prb: " + str(prb) + " and gain: " + str(gain))
            
