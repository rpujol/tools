#!/usr/bin/env python3
import matplotlib.pyplot as plt
from matplotlib import rc
import matplotlib
import csv
import numpy as np
import sys
import os
import re
# from matplotlib.colors import BoundaryNorm
# from matplotlib.ticker import MaxNLocator

IP_LIST = ['172.16.0.2','172.16.0.3','172.16.0.4','172.16.0.5','172.16.0.6','172.16.0.7']
PORT_1 = '5002'
PORT_2 = '5003'


def Average(lst): 
    return sum(lst) / len(lst) 


#MAIN
if __name__ == "__main__":

    #Dir navigation
    PRB_dir = []
    RUN_dir = []
    ROOT = os.getcwd() + "/"

    #Average Lists
    avg_downrate = []
    y_downrate_axis = []
    avg_uprate = []
    y_uprate_axis = []
    avg_mcs = []
    y_mcs_axis = []
    avg_snr = []
    y_snr_axis = []
    avg_rsrp = []
    y_rsrp_axis = []

    #Plot list
    x_axis = []
    label_x_axis = []
    label_prb_axis = []
    label_gain_axis = []
    label_down_count = []
    label_up_count = []


    #Vérifying that dir is valid
    try:
        os.chdir(ROOT + str(sys.argv[1])) 
        ROOT = os.getcwd() + "/"
    except:
        print("Invalid argument: Place yourself above the root dir to parse and give it as an argument")

    # PRB and Gain Dirs
    try:
        PRB_dir = os.listdir()
        PRB_dir.sort()
        #print (PRB_dir)
    except:
        print("Failed to list dir in this directory : " + os.getcwd())

    x_axis = PRB_dir

    for prb in PRB_dir :

        #Going to PRB dir
        try:
            os.chdir(ROOT + prb) 
        except:
                print("Something wrong here : " + ROOT + prb)

        # RUN Dirs
        try:
            RUN_dir = os.listdir()
            RUN_dir.sort()
        except:
            print("Failed to list dir in this directory : " + os.getcwd())

        succes_down_count = 0
        succes_up_count = 0
        avg_mcs = []
        avg_snr = []
        avg_rsrp = []
        for run in RUN_dir :

            #Going to RUN dir
            try:
                os.chdir(ROOT + prb + "/" + run) 
            except:
                print("Something wrong here : " + ROOT + prb + "/" + run)

            with open('./iperfserver.txt', 'r') as file :

                Lines = file.readlines()

                downlink = "lol"
                uplink = "lol"
                downrate = 0.
                uprate = 0.

                for line in Lines :

                    array = line.split()
                    if len(array) >= 8:
                        if array[2] == "local":
                            if (array[3] in IP_LIST) :
                                if ((array[5] == PORT_1) | (array[5] == PORT_2)):
                                    downlink = array[1]
                                else :
                                    print("bite")
                                    uplink = array[1]          
                        elif (array[3] == "sec") | (array[4] == "sec"):
                            if array[1] == downlink:
                                succes_down_count += 1
                                if array[7] == "Mbits/sec":
                                    downrate = float(array[6])
                                elif array[7] == "Kbits/sec":
                                    downrate = float(array[6]) / 1000
                                if len(array) > 8 :
                                    if array[8] == "Mbits/sec":
                                        downrate = float(array[7])
                                    elif array[8] == "Kbits/sec":
                                        downrate = float(array[7]) / 1000

                                try :
                                    ### GET MCS AND SNR from ue.log since iperf was successfull
                                    with open('./ue.log', 'r') as file :

                                        check1 = "PDCCH:"
                                        check2 = "dB"
                                        check3 = "MEAS:"
                                        check4 = "(serving),"
                                        mcs_list = []
                                        snr_list = []
                                        rsrp_list = []

                                        Lines = file.readlines()

                                        for line in Lines :
                                            array = line.split()
                                            if len(array) >= 17:
                                                if (array[5] == check1) & (array[16] == check2):
                                                    mcs = str(array[12])
                                                    snr = str(array[15])
                                                    mcs = float(mcs[5:-2])
                                                    snr = float(snr[4:])
                                                    mcs_list.append(mcs)
                                                    snr_list.append(snr)
                                            if len(array) == 12:
                                                if (array[4] == check3) & (array[9] == check4):
                                                    rsrp = str(array[10])
                                                    rsrp = float(rsrp[5:])
                                                    rsrp_list.append(rsrp)

                                        if mcs_list != [] :
                                            avg_mcs.append(Average(mcs_list))
                                        if snr_list != [] :
                                            avg_snr.append(Average(snr_list))
                                        if rsrp_list != [] :
                                            avg_rsrp.append(Average(rsrp_list))
                                except:
                                    print("Pas de ue.log dans cette expérimentation")
                                ### END MCS SNR

                            elif array[1] == uplink:
                                succes_up_count += 1
                                if array[7] == "Mbits/sec":
                                    uprate = float(array[6])
                                elif array[7] == "Kbits/sec":
                                    uprate = float(array[6]) / 1000
                                if len(array) > 8 :
                                    if array[8] == "Mbits/sec":
                                        uprate = float(array[7])
                                    elif array[8] == "Kbits/sec":
                                        uprate = float(array[7]) / 1000
  
                        else :
                            pass
                    else :
                        pass

                    if downrate != 0. :
                        avg_downrate.append(downrate)
                    if uprate != 0. :
                        avg_uprate.append(uprate)

        # print(prb)
        label_down_count.append(succes_down_count)
        label_up_count.append(succes_up_count)
        phrase = prb
        extraction = re.match(r'(.*)prb(.*)gain', phrase)
        prblocks = extraction.group(1)
        gain = extraction.group(2)
        label_x_axis.append(prb)
        if prblocks not in label_prb_axis:
            label_prb_axis.append(prblocks)
        if gain not in label_gain_axis:
            label_gain_axis.append(gain)

        #DOWNLINK RATE
        if avg_downrate != [] :
            average = Average(avg_downrate)
            y_downrate_axis.append(average)
            avg_downrate = []
        else:
            y_downrate_axis.append(0)

        #UPLINK RATE
        if avg_uprate != [] :
            average = Average(avg_uprate)
            y_uprate_axis.append(average)
            avg_uprate = []
        else:
            y_uprate_axis.append(0)

        if avg_mcs != [] :
            # MCS SNR
            mcs = Average(avg_mcs)
            y_mcs_axis.append(mcs)
        else:
            y_mcs_axis.append(0)
        if avg_snr != [] :
            snr = Average(avg_snr)
            y_snr_axis.append(snr)
        else:
            y_snr_axis.append(0)
        if avg_rsrp != [] :
            rsrp = Average(avg_rsrp)
            y_rsrp_axis.append(rsrp)
        else:
            y_rsrp_axis.append(0)

    # REPLACE 100PRB TO THE END
    for i in PRB_dir:
        if "100prb" in i:
            y_downrate_axis.append(y_downrate_axis.pop(0))
            label_down_count.append(label_down_count.pop(0))
            y_uprate_axis.append(y_uprate_axis.pop(0))
            label_up_count.append(label_up_count.pop(0))
            y_mcs_axis.append(y_mcs_axis.pop(0))
            y_snr_axis.append(y_snr_axis.pop(0))
            y_rsrp_axis.append(y_rsrp_axis.pop(0))

    ### FIGURE ALL###
    fig = plt.figure()
    label_prb_axis.append(label_prb_axis.pop(0))

    ### FIGURE 1 ###
    ax = fig.add_subplot()
    ax.set_title('Average Rate in Mbits/sec')
    plt.xticks(rotation='vertical')

    if y_downrate_axis != [] :

        ### FIGURE 1 ###
        # print(x_axis)
        # print(y_downrate_axis)
        ax.plot(x_axis, y_downrate_axis, label= ("Downlink rate"), marker="x")
        ax.plot(x_axis, label_down_count, label= ("Downlink #success"))
        ax.set_xlabel('PRB and GAIN Configurations')
        ax.set_ylabel('Rate in Mbits/sec')
        ax.set_title('Average Rate in Mbits/sec')
        ax.set_ylim([0,51])
        ax.legend(loc=2)
        fig.savefig('/home/rpujol/Experimentations/Chap2successonlycut10avg/Courbes/debit_reussite.png')


        # ### FIGURE 2 ###
        # fig, ax12 = plt.subplots()
        # Z = np.array([y_downrate_axis[0:len(label_gain_axis)],
        #     y_downrate_axis[len(label_gain_axis):len(label_gain_axis)*2],
        #     y_downrate_axis[len(label_gain_axis)*2:len(label_gain_axis)*3],
        #     y_downrate_axis[len(label_gain_axis)*3:len(label_gain_axis)*4]])

        # label_prb_axis.insert(0,0)
        # label_gain_axis.insert(0,0)
        # ax12.pcolormesh(label_gain_axis, label_prb_axis, Z, cmap = "Reds") # , vmin=7, vmax = 60
        # label_prb_axis.pop(0)
        # label_gain_axis.pop(0)
        # ax12.set_title('PRB vs GAIN Downlink matrix')
        # fig.savefig('/home/rpujol/Experimentations/Chap2/Courbes/matrice_debit1.png')

        ### FIGURE 3 ###
        fig, ax13 = plt.subplots()
        Z = np.array([y_downrate_axis[0:len(label_gain_axis)],
            y_downrate_axis[len(label_gain_axis):len(label_gain_axis)*2],
            y_downrate_axis[len(label_gain_axis)*2:len(label_gain_axis)*3],
            y_downrate_axis[len(label_gain_axis)*3:len(label_gain_axis)*4]])
        ax13.matshow(Z, cmap='Reds', origin='lower', vmin=0, vmax=51)
        for (i, j), z in np.ndenumerate(Z):
            ax13.text(j, i, '{:0.1f}'.format(z), ha='center', va='center',
                    bbox=dict(boxstyle='round', facecolor='white', edgecolor='0.3'))
        label_prb_axis.insert(0,0)
        label_gain_axis.insert(0,0)
        ax13.set_xticklabels(label_gain_axis, rotation=90)
        ax13.set_yticklabels(label_prb_axis)
        label_prb_axis.pop(0)
        label_gain_axis.pop(0)
        ax13.set_title('PRB vs GAIN Downlink matrix')
        fig.savefig('/home/rpujol/Experimentations/Chap2successonlycut10avg/Courbes/matrice_debit.png')

        if y_mcs_axis != [] :
            ### FIGURE 4 ###
            fig, ax14 = plt.subplots()
            Z14 = np.array([y_mcs_axis[0:len(label_gain_axis)],
                y_mcs_axis[len(label_gain_axis):len(label_gain_axis)*2],
                y_mcs_axis[len(label_gain_axis)*2:len(label_gain_axis)*3],
                y_mcs_axis[len(label_gain_axis)*3:len(label_gain_axis)*4]])
            ax14.matshow(Z14, cmap='Reds', origin='lower', vmin=0, vmax=31)
            for (i, j), z in np.ndenumerate(Z14):
                ax14.text(j, i, '{:0.1f}'.format(z), ha='center', va='center',
                        bbox=dict(boxstyle='round', facecolor='white', edgecolor='0.3'))
            label_prb_axis.insert(0,0)
            label_gain_axis.insert(0,0)
            ax14.set_xticklabels(label_gain_axis, rotation=90)
            ax14.set_yticklabels(label_prb_axis)
            label_prb_axis.pop(0)
            label_gain_axis.pop(0)
            ax14.set_title('PRB vs GAIN mcs matrix')
            fig.savefig('/home/rpujol/Experimentations/Chap2successonlycut10avg/Courbes/matrice_mcs.png')

        if y_snr_axis != [] :
            ### FIGURE 5 ###
            fig, ax15 = plt.subplots()
            Z15 = np.array([y_snr_axis[0:len(label_gain_axis)],
                y_snr_axis[len(label_gain_axis):len(label_gain_axis)*2],
                y_snr_axis[len(label_gain_axis)*2:len(label_gain_axis)*3],
                y_snr_axis[len(label_gain_axis)*3:len(label_gain_axis)*4]])
            ax15.matshow(Z15, cmap='Reds', origin='lower', vmin=0, vmax=36)
            for (i, j), z in np.ndenumerate(Z15):
                ax15.text(j, i, '{:0.1f}'.format(z), ha='center', va='center',
                        bbox=dict(boxstyle='round', facecolor='white', edgecolor='0.3'))
            label_prb_axis.insert(0,0)
            label_gain_axis.insert(0,0)
            ax15.set_xticklabels(label_gain_axis, rotation=90)
            ax15.set_yticklabels(label_prb_axis)
            label_prb_axis.pop(0)
            label_gain_axis.pop(0)                
            ax15.set_title('PRB vs GAIN snr matrix')
            fig.savefig('/home/rpujol/Experimentations/Chap2successonlycut10avg/Courbes/matrice_snr.png')

        if y_rsrp_axis != [] :
            ### FIGURE 6 ###
            fig, ax16 = plt.subplots()
            Z16 = np.array([y_rsrp_axis[0:len(label_gain_axis)],
                y_rsrp_axis[len(label_gain_axis):len(label_gain_axis)*2],
                y_rsrp_axis[len(label_gain_axis)*2:len(label_gain_axis)*3],
                y_rsrp_axis[len(label_gain_axis)*3:len(label_gain_axis)*4]])
            ax16.matshow(Z16, cmap='Reds', origin='lower')
            for (i, j), z in np.ndenumerate(Z16):
                ax16.text(j, i, '{:0.1f}'.format(z), ha='center', va='center',
                        bbox=dict(boxstyle='round', facecolor='white', edgecolor='0.3'))
            label_prb_axis.insert(0,0)
            label_gain_axis.insert(0,0)
            ax16.set_xticklabels(label_gain_axis, rotation=90)
            ax16.set_yticklabels(label_prb_axis)
            label_prb_axis.pop(0)
            label_gain_axis.pop(0)                
            ax16.set_title('PRB vs GAIN rsrp matrix')
            for val in y_rsrp_axis:
                print(str(val), end = ',')
            fig.savefig('/home/rpujol/Experimentations/Chap2successonlycut10avg/Courbes/matrice_rsrp.png')



    # if y_uprate_axis[] != [] :

    #     ### FIGURE 1 ###
    #     ax.plot(x_axis, y_uprate_axis, label= ("Uplink rate"))
    #     ax.plot(x_axis, label_up_count, label= ("Uplink #success"))

    #     ### FIGURE 4 ###
    #     fig, ax22 = plt.subplots()
    #     print(y_uprate_axis)
    #     Z2 = np.array([y_uprate_axis[0:len(label_gain_axis)],
    #         y_uprate_axis[len(label_gain_axis):len(label_gain_axis)*2],
    #         y_uprate_axis[len(label_gain_axis)*2:len(label_gain_axis)*3],
    #         y_uprate_axis[len(label_gain_axis)*3:len(label_gain_axis)*4]])
    #     label_prb_axis.insert(0,0)
    #     label_gain_axis.insert(0,0)
    #     ax22.pcolormesh(label_gain_axis, label_prb_axis, Z2, cmap = "Reds") # , vmin=7, vmax = 60
    #     label_prb_axis.pop(0)
    #     label_gain_axis.pop(0)
    #     ax22.set_title('PRB vs GAIN Uplink matrix')

    #     ### FIGURE 5 ###
    #     fig, ax23 = plt.subplots()
    #     # Z = np.array([y_downrate_axis[len(label_gain_axis):len(label_gain_axis)*2],
    #     #     y_downrate_axis[len(label_gain_axis)*2:len(label_gain_axis)*3],
    #     #     y_downrate_axis[len(label_gain_axis)*3:len(label_gain_axis)*4],
    #     #     y_downrate_axis[0:len(label_gain_axis)]])
    #     ax23.matshow(Z2, cmap='Reds', origin = "lower")
    #     for (i, j), z in np.ndenumerate(Z2):
    #         ax23.text(j, i, '{:0.1f}'.format(z), ha='center', va='center',
    #                 bbox=dict(boxstyle='round', facecolor='white', edgecolor='0.3'))
    #     label_prb_axis.insert(0,0)
    #     label_gain_axis.insert(0,0)
    #     ax23.set_xticklabels(label_gain_axis, rotation=90)
    #     ax23.set_yticklabels(label_prb_axis)
    #     label_prb_axis.pop(0)
    #     label_gain_axis.pop(0) 
    #     ax23.set_title('PRB vs GAIN Uplink matrix')

    ax.grid(linestyle='--', linewidth=0.5)
    plt.show()
