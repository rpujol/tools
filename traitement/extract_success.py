#!/usr/bin/env python3
import matplotlib.pyplot as plt
from matplotlib import rc
import matplotlib
import csv
import numpy as np
import sys
import os
import re
# from matplotlib.colors import BoundaryNorm
# from matplotlib.ticker import MaxNLocator

IP_LIST = ['172.16.0.2','172.16.0.3','172.16.0.4','172.16.0.5','172.16.0.6','172.16.0.7']
PORT_1 = '5002'
PORT_2 = '5003'




#MAIN
if __name__ == "__main__":

    BASE_ROOT = os.getcwd() + "/"
    ROOT = ""
    args_list = []
    final_list = []
    i=0
    j=0
    k=0

    #Vérifying that dir is valid
    for args in sys.argv[1:]:

        #Dir navigation
        PRB_dir = []
        RUN_dir = []
        list_list = []
        temp_list = []

        try:
            os.chdir(BASE_ROOT + str(args)) 
            ROOT = os.getcwd() + "/"
            print("current dir : " + os.getcwd())

            # PRB and Gain Dirs
            try:
                PRB_dir = os.listdir()
                PRB_dir.sort()
                print (PRB_dir)
            except:
                print("Failed to list dir in this directory : " + os.getcwd())

            x_axis = PRB_dir

            for prb in PRB_dir :

                #Going to PRB dir
                try:
                    os.chdir(ROOT + prb) 
                except:
                        print("Something wrong here : " + ROOT + prb)

                # RUN Dirs
                try:
                    RUN_dir = os.listdir()
                    RUN_dir.sort()
                except:
                    print("Failed to list dir in this directory : " + os.getcwd())

                temp_list = []
                for run in RUN_dir :
                    succes_down_count = 0

                    #Going to PRB dir
                    try:
                        os.chdir(ROOT + prb + "/" + run) 
                    except:
                        print("Something wrong here : " + ROOT + prb + "/" + run)

                    with open('./iperfserver.txt', 'r') as file :

                        Lines = file.readlines()
                        for line in Lines :
                            array = line.split()
                            if len(array) >= 8:
                                if (array[3] == "sec") | (array[4] == "sec"):
                                    temp_list.append(1)
                                    succes_down_count=1
                    if succes_down_count == 0:                
                        temp_list.append(0)
                list_list.append(temp_list)
            args_list.append(list_list)
        except:
            print("Invalid argument: " + args + " Place yourself above the root dir to parse and give it as an argument")
    print(args_list)
    i = len(args_list)
    j = len(args_list[0])
    k = len(args_list[0][0])
    print( i, j ,k)
    for arg in sys.argv[1:]:
        try:
            os.chdir("/home/rpujol/Experimentations/Chap2successonly/")
            os.system("mkdir /home/rpujol/Experimentations/Chap2successonly/"+arg)
        except:
            print("XUEX")

    for m in range(j):
        for arg in sys.argv[1:]:
            try:
                os.chdir("/home/rpujol/Experimentations/Chap2successonly/"+arg)
                os.system("mkdir /home/rpujol/Experimentations/Chap2successonly/"+arg+PRB_dir[m])
            except:
                print("prbgain")

        for n in range(k):
            stop = 0
            for l in range(i):
                if args_list[l][m][n] == 0:
                    stop = 1
            if stop == 0:
                for arg in sys.argv[1:]:
                    try:
                        os.chdir("/home/rpujol/Experimentations/Chap2successonly/"+arg+PRB_dir[m])
                    except:
                        print("fail")
                    try:
                        os.system("cp -r "+BASE_ROOT+arg+PRB_dir[m]+"/"+RUN_dir[n]+ " "+"/home/rpujol/Experimentations/Chap2successonly/"+arg+PRB_dir[m]+"/"+RUN_dir[n]+"reste")
                    except:
                        print("cp fail")