#!/usr/bin/env python3
import matplotlib.pyplot as plt
from matplotlib import rc
import matplotlib
import csv
import numpy as np
import sys
import os
import re
# from matplotlib.colors import BoundaryNorm
# from matplotlib.ticker import MaxNLocator

IP_LIST = ['172.16.0.2','172.16.0.3','172.16.0.4','172.16.0.5','172.16.0.6','172.16.0.7']
PORT_1 = '5002'
PORT_2 = '5003'


def Average(lst): 
    return sum(lst) / len(lst) 


#MAIN
if __name__ == "__main__":

    BASE_ROOT = os.getcwd() + "/"
    ROOT = ""
    ### FIGURE 1 ###
    fig = plt.figure()
    ax = fig.add_subplot()
    ax.set_title('Average Rate in Mbits/sec')
    plt.xticks(rotation='vertical')

    #Vérifying that dir is valid
    for args in sys.argv:

        #Dir navigation
        PRB_dir = []
        RUN_dir = []

        #Average Lists
        avg_downrate = []
        y_downrate_axis = []
        avg_uprate = []
        y_uprate_axis = []
        avg_mcs = []
        y_mcs_axis = []
        avg_snr = []
        y_snr_axis = []

        #Plot list
        x_axis = []
        label_x_axis = []
        label_prb_axis = []
        label_gain_axis = []
        label_down_count = []
        label_up_count = []

        try:
            os.chdir(BASE_ROOT + str(args)) 
            ROOT = os.getcwd() + "/"
            print("current dir : " + os.getcwd())

            # PRB and Gain Dirs
            try:
                PRB_dir = os.listdir()
                PRB_dir.sort()
                print (PRB_dir)
            except:
                print("Failed to list dir in this directory : " + os.getcwd())

            x_axis = PRB_dir

            for prb in PRB_dir :

                #Going to PRB dir
                try:
                    os.chdir(ROOT + prb) 
                except:
                        print("Something wrong here : " + ROOT + prb)

                # RUN Dirs
                try:
                    RUN_dir = os.listdir()
                    RUN_dir.sort()
                    #print(RUN_dir)
                except:
                    print("Failed to list dir in this directory : " + os.getcwd())

                succes_down_count = 0
                succes_up_count = 0
                for run in RUN_dir :

                    #Going to PRB dir
                    try:
                        os.chdir(ROOT + prb + "/" + run) 
                    except:
                        print("Something wrong here : " + ROOT + prb + "/" + run)

                    #print(os.getcwd())

                    with open('./iperfserver.txt', 'r') as file :

                        Lines = file.readlines()

                        downlink = "lol"
                        uplink = "lol"
                        downrate = 0.
                        uprate = 0.

                        for line in Lines :

                            array = line.split()
                            if len(array) >= 8:
                                #print(array)
                                if array[2] == "local":
                                    if (array[3] in IP_LIST) :
                                        if ((array[5] == PORT_1) | (array[5] == PORT_2)):
                                            downlink = array[1]
                                        else :
                                            #print("bite")
                                            uplink = array[1]          
                                elif (array[3] == "sec") | (array[4] == "sec"):
                                    if array[1] == downlink:
                                        succes_down_count += 1
                                        if array[7] == "Mbits/sec":
                                            downrate = float(array[6])
                                        elif array[7] == "Kbits/sec":
                                            downrate = float(array[6]) / 1000
                                        if len(array) > 8 :
                                            if array[8] == "Mbits/sec":
                                                downrate = float(array[7])
                                            elif array[8] == "Kbits/sec":
                                                downrate = float(array[7]) / 1000

                                        # try :
                                        #     ### GET MCS AND SNR from ue.log since iperf was successfull
                                        #     with open('./ue.log', 'r') as file :

                                        #         check1 = "PDCCH:"
                                        #         check2 = "dB"
                                        #         mcs_list = []
                                        #         snr_list = []

                                        #         Lines = file.readlines()

                                        #         for line in Lines :
                                        #             array = line.split()
                                        #             if len(array) >= 17:
                                        #                 if (array[5] == check1) & (array[16] == check2):
                                        #                     mcs = str(array[12])
                                        #                     snr = str(array[15])
                                        #                     mcs = float(mcs[5:-2])
                                        #                     snr = float(snr[4:])
                                        #                     mcs_list.append(mcs)
                                        #                     snr_list.append(snr)

                                        #         if mcs_list != [] :
                                        #             avg_mcs.append(Average(mcs_list))
                                        #         if snr_list != [] :
                                        #             avg_snr.append(Average(snr_list))
                                        # except:
                                        #     print("Pas de ue.log dans cette expérimentation")
                                        ### END MCS SNR
                                    elif array[1] == uplink:
                                        succes_up_count += 1
                                        if array[7] == "Mbits/sec":
                                            uprate = float(array[6])
                                        elif array[7] == "Kbits/sec":
                                            uprate = float(array[6]) / 1000
                                        if len(array) > 8 :
                                            if array[8] == "Mbits/sec":
                                                uprate = float(array[7])
                                            elif array[8] == "Kbits/sec":
                                                uprate = float(array[7]) / 1000 
                                else :
                                    pass
                            else :
                                pass

                            if downrate != 0. :
                                avg_downrate.append(downrate)
                            if uprate != 0. :
                                avg_uprate.append(uprate)

                # print(prb)
                label_down_count.append(succes_down_count)
                label_up_count.append(succes_up_count)
                phrase = prb
                extraction = re.match(r'(.*)prb(.*)gain', phrase)
                prblocks = extraction.group(1)
                gain = extraction.group(2)
                label_x_axis.append(prb)
                if prblocks not in label_prb_axis:
                    label_prb_axis.append(prblocks)
                if gain not in label_gain_axis:
                    label_gain_axis.append(gain)

                #DOWNLINK RATE
                if avg_downrate != [] :
                    average = Average(avg_downrate)
                    y_downrate_axis.append(average)
                    avg_downrate = []
                else:
                    y_downrate_axis.append(0)

                #UPLINK RATE
                if avg_uprate != [] :
                    average = Average(avg_uprate)
                    y_uprate_axis.append(average)
                    avg_uprate = []
                else:
                    y_uprate_axis.append(0)

                if avg_mcs != [] :
                    # MCS SNR
                    mcs = Average(avg_mcs)
                    y_mcs_axis.append(mcs)
                else:
                    y_mcs_axis.append(0)
                if avg_snr != [] :
                    snr = Average(avg_snr)
                    y_snr_axis.append(snr)
                else:
                    y_snr_axis.append(0)

            # REPLACE 100PRB TO THE END
            for i in PRB_dir:
                if "100prb" in i:
                    y_downrate_axis.append(y_downrate_axis.pop(0))
                    label_down_count.append(label_down_count.pop(0))
                    y_uprate_axis.append(y_uprate_axis.pop(0))
                    label_up_count.append(label_up_count.pop(0))

            if y_downrate_axis != [] :

                ### FIGURE 1 ###
                xx_axis = x_axis[7:]+x_axis[:7]
                ax.plot(xx_axis, y_downrate_axis, label= (args + " : Downlink"), marker="x")
                ax.set_xlabel('PRB and GAIN Configurations')
                ax.set_ylabel('Rate in Mbits/sec')
                ax.set_title('Average Rate in Mbits/sec')
                ax.set_ylim([0,51])
                #ax.plot(xx_axis, label_down_count, label= (args + " : #succes"))
                for val in y_downrate_axis:
                    print(str(val), end = ',')


            # if y_uprate_axis != [] :

            #     ### FIGURE 1 ###
            #     ax.plot(x_axis, y_uprate_axis, label= (args + " : Uplink"))
            #     ax.plot(x_axis, label_up_count, label= (args + " : #succes"))

        except:
            print("Invalid argument: " + args + " Place yourself above the root dir to parse and give it as an argument")


    ax.legend(loc=2)
    plt.grid(linestyle='--', linewidth=0.5)
    plt.show()
    #fig.savefig(BASE_ROOT + 'all_rate.png')